from django.db import models
from django.db.models.fields import CharField

# Create your models here.
class Note(models.Model):
    to = CharField(max_length=20)
    fromInp =CharField(max_length=20)
    title = CharField(max_length=20)
    message=CharField(max_length=200)
