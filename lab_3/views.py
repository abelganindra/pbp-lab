from django.shortcuts import render,redirect
from lab_1.models import Friend
from .forms import FriendForm
from django.contrib.auth.decorators import login_required
# Create your views here.
@login_required(login_url='/admin/login/')
def index(request):
    friends = Friend.objects.all().values()  # TODO Implement this
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)

def add_friend(request):
    form = FriendForm(request.POST)
    if form.is_valid():
        form.save()
        return redirect('/lab-3')
    context = {'form': form}
    return render(request,'lab3_form.html',context)
